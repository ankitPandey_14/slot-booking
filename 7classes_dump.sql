--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: availability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.availability (
    tut_id integer NOT NULL,
    subject_id integer NOT NULL,
    batch_id integer NOT NULL,
    day text NOT NULL,
    start_at text NOT NULL,
    end_at text NOT NULL
);


ALTER TABLE public.availability OWNER TO postgres;

--
-- Name: batches; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.batches (
    batch_id integer NOT NULL,
    batch_name text NOT NULL
);


ALTER TABLE public.batches OWNER TO postgres;

--
-- Name: students; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students (
    stu_id integer NOT NULL,
    stu_first_name text NOT NULL,
    stu_last_name text,
    stu_email_id text NOT NULL,
    stu_contact_number bigint NOT NULL,
    have_laptop boolean NOT NULL,
    batch_id integer NOT NULL
);


ALTER TABLE public.students OWNER TO postgres;

--
-- Name: subjects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subjects (
    subject_id integer NOT NULL,
    subject_name text NOT NULL
);


ALTER TABLE public.subjects OWNER TO postgres;

--
-- Name: teachers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teachers (
    tut_id integer NOT NULL,
    tut_first_name text NOT NULL,
    tut_last_name text,
    tut_email_id text NOT NULL,
    tut_contact_number bigint NOT NULL,
    subject_id integer NOT NULL
);


ALTER TABLE public.teachers OWNER TO postgres;

--
-- Name: timetable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.timetable (
    tut_id integer NOT NULL,
    subject_name text NOT NULL,
    day text NOT NULL,
    start_at text NOT NULL,
    end_at text NOT NULL
);


ALTER TABLE public.timetable OWNER TO postgres;

--
-- Data for Name: availability; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.availability (tut_id, subject_id, batch_id, day, start_at, end_at) FROM stdin;
1	2	2	Monday	9:00:00	10:00:00
1	2	2	Monday	10:00:00	11:00:00
1	2	2	Monday	14:00:00	15:00:00
1	2	2	Monday	15:00:00	16:00:00
1	2	2	Monday	16:00:00	17:00:00
1	2	2	Tuesday	9:00:00	10:00:00
1	2	2	Tuesday	10:00:00	11:00:00
1	2	2	Tuesday	11:00:00	12:00:00
1	2	2	Tuesday	12:00:00	13:00:00
1	2	2	Tuesday	15:00:00	16:00:00
1	2	2	Tuesday	16:00:00	17:00:00
1	2	2	Tuesday	17:00:00	18:00:00
1	2	2	Tuesday	18:00:00	19:00:00
1	2	2	Tuesday	19:00:00	20:00:00
1	2	2	Wednesday	11:00:00	12:00:00
1	2	2	Wednesday	12:00:00	13:00:00
1	2	2	Wednesday	14:00:00	15:00:00
1	2	2	Wednesday	15:00:00	16:00:00
1	2	2	Wednesday	16:00:00	17:00:00
1	2	2	Wednesday	17:00:00	18:00:00
1	2	2	Wednesday	18:00:00	19:00:00
1	2	2	Thursday	16:00:00	17:00:00
1	2	2	Thursday	17:00:00	18:00:00
1	2	2	Thursday	20:00:00	21:00:00
1	2	2	Friday	8:00:00	9:00:00
1	2	2	Friday	9:00:00	10:00:00
1	2	2	Friday	10:00:00	11:00:00
1	2	2	Friday	14:00:00	15:00:00
1	2	2	Saturday	10:00:00	11:00:00
1	2	2	Saturday	13:00:00	14:00:00
1	2	2	Sunday	9:00:00	10:00:00
1	2	2	Sunday	10:00:00	11:00:00
1	2	2	Sunday	11:00:00	12:00:00
1	2	2	Sunday	14:00:00	15:00:00
2	3	4	Monday	10:00:00	11:00:00
2	3	4	Monday	11:00:00	12:00:00
2	3	4	Monday	15:00:00	16:00:00
2	3	4	Monday	16:00:00	17:00:00
2	3	4	Wednesday	10:00:00	11:00:00
2	3	4	Wednesday	11:00:00	12:00:00
2	3	4	Wednesday	12:00:00	13:00:00
2	3	4	Wednesday	15:00:00	16:00:00
2	3	4	Wednesday	16:00:00	17:00:00
2	3	4	Wednesday	17:00:00	18:00:00
2	3	4	Thursday	16:00:00	17:00:00
2	3	4	Thursday	17:00:00	18:00:00
2	3	4	Thursday	20:00:00	21:00:00
2	3	4	Friday	8:00:00	9:00:00
2	3	4	Friday	9:00:00	10:00:00
2	3	4	Friday	10:00:00	11:00:00
2	3	4	Friday	14:00:00	15:00:00
2	3	4	Saturday	9:00:00	10:00:00
2	3	4	Saturday	12:00:00	13:00:00
2	3	4	Saturday	13:00:00	14:00:00
2	3	4	Sunday	10:00:00	11:00:00
2	3	4	Sunday	11:00:00	12:00:00
2	3	4	Sunday	15:00:00	16:00:00
2	3	4	Sunday	16:00:00	17:00:00
\.


--
-- Data for Name: batches; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.batches (batch_id, batch_name) FROM stdin;
1	4th
2	5th
3	6th
4	7th
5	8th
6	9th
7	10th
8	11th
9	12th
10	13th(Drop Year)
11	College Student
12	Technology Training
13	Employee Training
\.


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.students (stu_id, stu_first_name, stu_last_name, stu_email_id, stu_contact_number, have_laptop, batch_id) FROM stdin;
1	Anoop	Pandey	anoop@gmail.com	8754690325	t	9
2	Rohit	Singh	rohit@gmail.com	9657840126	f	2
\.


--
-- Data for Name: subjects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subjects (subject_id, subject_name) FROM stdin;
1	Physics
2	Chemistry
3	Maths
4	Biology
5	SST
6	English
7	Biotechnology
8	Science
9	Hindi
10	Sanskrit
11	React JS
12	Python
13	AI/ML
14	DevOps
15	Backened
16	Interview Prep
17	English Speaking
18	Employee Training
\.


--
-- Data for Name: teachers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.teachers (tut_id, tut_first_name, tut_last_name, tut_email_id, tut_contact_number, subject_id) FROM stdin;
1	Mohit	Saxena	mohit@gmail.com	7845963201	1
2	Karan		karan@gmail.com	9865743012	11
3	Aman	Kumar	kumaraman@gmail.com	7458321069	5
\.


--
-- Data for Name: timetable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.timetable (tut_id, subject_name, day, start_at, end_at) FROM stdin;
2	Maths	Sunday	17:00:00	18:00:00
\.


--
-- Name: batches batches_batch_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batches
    ADD CONSTRAINT batches_batch_name_key UNIQUE (batch_name);


--
-- Name: batches batches_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.batches
    ADD CONSTRAINT batches_pkey PRIMARY KEY (batch_id);


--
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (stu_id);


--
-- Name: students students_stu_contact_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_stu_contact_number_key UNIQUE (stu_contact_number);


--
-- Name: subjects subjects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subjects
    ADD CONSTRAINT subjects_pkey PRIMARY KEY (subject_id);


--
-- Name: subjects subjects_subject_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subjects
    ADD CONSTRAINT subjects_subject_name_key UNIQUE (subject_name);


--
-- Name: teachers teachers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_pkey PRIMARY KEY (tut_id);


--
-- Name: teachers teachers_tut_contact_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_tut_contact_number_key UNIQUE (tut_contact_number);


--
-- Name: teachers teachers_tut_email_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT teachers_tut_email_id_key UNIQUE (tut_email_id);


--
-- Name: availability fk_availability_batches; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT fk_availability_batches FOREIGN KEY (batch_id) REFERENCES public.batches(batch_id);


--
-- Name: availability fk_availability_subjects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT fk_availability_subjects FOREIGN KEY (subject_id) REFERENCES public.subjects(subject_id);


--
-- Name: availability fk_availability_teachers; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT fk_availability_teachers FOREIGN KEY (tut_id) REFERENCES public.teachers(tut_id);


--
-- Name: students fk_students_batches; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT fk_students_batches FOREIGN KEY (batch_id) REFERENCES public.batches(batch_id);


--
-- Name: teachers fk_teachers_subjects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teachers
    ADD CONSTRAINT fk_teachers_subjects FOREIGN KEY (subject_id) REFERENCES public.subjects(subject_id);


--
-- Name: timetable fk_timetable_teachers; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timetable
    ADD CONSTRAINT fk_timetable_teachers FOREIGN KEY (tut_id) REFERENCES public.teachers(tut_id);


--
-- PostgreSQL database dump complete
--

