1. Batches (batches_ins.csv)

Can fill data in the given csv (batches_ins.csv) file according to the mentioned columns and Save the file.
"batch_id" refers to uniquely identify each batch.

For inserting the data into the database execute the python file "batch.py"
>>> python batch.py

 batch_id | batch_name
----------+------------
        1 | 4th
        2 | 5th
        3 | 6th
        4 | 7th
        5 | 8th


-----------------------------------------------------------------------------------------------



2. Subjects (subjects_ins.csv)

Can fill data in the given csv (subjects_ins.csv) file according to the mentioned columns and Save the file.
"subject_id" refers to uniquely identify each subject.

For inserting the data into the database execute the python file "sub.py"
>>> python sub.py

 subject_id | subject_name
------------+--------------
          1 | Physics
          2 | Chemistry
          3 | Maths
          4 | Biology
          5 | SST


-----------------------------------------------------------------------------------------------



3. Students (students_ins.csv)

Can fill data in the given csv (students_ins.csv) file according to the mentioned columns and Save the File.
"stu_id" refers to uniquely identify each student.
"batch_id" is the reference to the "batches" table

For inserting the data into the database execute the python file "stud.py"
>>> python stud.py

 stu_id | stu_first_name | stu_last_name |  stu_email_id   | stu_contact_number | have_laptop | batch_id
--------+----------------+---------------+-----------------+--------------------+-------------+----------
      1 | Anoop          | Pandey        | anoop@gmail.com |         8754690325 | t           |        9
      2 | Rohit          | Singh         | rohit@gmail.com |         9657840126 | f           |        2


-----------------------------------------------------------------------------------------------



4. Teachers (teachers_ins.csv)

Can fill data in the given csv (teachers_ins.csv) file according to the mentioned columns and Save the File.
"tut_id" refers to uniquely identify each teacher.
"subject_id" is the reference to the "subjects" table

For inserting the data into the database execute the python file "teach.py"
>>> python teach.py

 tut_id | tut_first_name | tut_last_name |    tut_email_id     | tut_contact_number | subject_id
--------+----------------+---------------+---------------------+--------------------+------------
      1 | Mohit          | Saxena        | mohit@gmail.com     |         7845963201 |          1
      2 | Karan          |               | karan@gmail.com     |         9865743012 |         11
      3 | Aman           | Kumar         | kumaraman@gmail.com |         7458321069 |          5


-----------------------------------------------------------------------------------------------




5. Availability (availability_ins.csv)

Can fill data in the given csv (availability_ins.csv) file according to the mentioned columns and Save the File.
"tut__id" is the reference to the "teachers" table.
"subject_id" is the reference to the "subjects" table
"batch_id" is the reference to the "batches" table

Enter the timmigs(according to 24 hour clock) of each day seperated by a comma.

For inserting the data into the database execute the python file "avail.py"
>>> python avail.py

 tut_id | subject_id | batch_id |   Monday    |   Tuesday   |  Wednessday  |   Thursday   |   Friday   |  Saturday  |  Sunday  
--------+------------+----------+------------+-------------+--------------+--------------+------------+------------+------------
      1 |          2 |        2 | 9-11,14-17  | 9-13,15-20  | 11-13,14-19  | 16-18,20-21  | 8-11,14-15 | 10-11,11-14 | 9-12,14-15
      2 |          3 |        4 | 10-12,15-17 |             | 10-13,15-18  | 16-18,20-21  | 8-11,14-15 | 9-10,12-14 | 10-12,15-18


Database Table
--------------
 tut_id | subject_id | batch_id |  day   | start_at |  end_at
--------+------------+----------+--------+----------+----------
      1 |          2 |        2 | Monday | 9:00:00  | 10:00:00
      1 |          2 |        2 | Monday | 10:00:00 | 11:00:00


-----------------------------------------------------------------------------------------------



6. Time Table (Database)

Execute this file "tim_tbl.py".
>>> python tim_tbl.py

Enter the subject name and the preferred day to book the slots.
Select From the given time slots.
And you are done with it.

 tut_id | subject_name |  day   | start_at |  end_at
--------+--------------+--------+----------+----------
      2 | Maths        | Sunday | 17:00:00 | 18:00:00