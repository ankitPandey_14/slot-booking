import csv
import psycopg2

# connect to the database
con = psycopg2.connect(host='localhost',
                       dbname='details',
                       user='postgres',
                       password='@NK|T14P@sTgre',
                       port=5432,
                       )


# cursor
cur = con.cursor()

cur.execute(f'SELECT * FROM availability')
rows = cur.fetchall()
a={rows[i][0] for i in range(len(rows))}



with open('availability_ins.csv', 'r') as af:
    reader = csv.reader(af)
    next(af)
    for row in reader:
        if int(row[0]) not in a:
            l = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
            index=0
            for i in range(3, len(row)):
                sp = row[i].split(',')
                for j in sp:
                    jsp = j.split('-')
                    if jsp!=[""]:
                        for k in range(int(jsp[0]), int(jsp[1])):
                            cur.execute("INSERT INTO availability VALUES (%s, %s, %s, %s, %s, %s)", [row[0], row[1], row[2], l[index], f"{k}:00:00", f"{k+1}:00:00"])
                index+=1





cur.execute(f'SELECT * FROM availability')

rows = cur.fetchall()
for row in rows:
    print(row)




# commit the transaction
con.commit()

# close the cursor
cur.close()

# close the connection
con.close()




# CREATE TABLE availability(avail_id INT PRIMARY KEY NOT NULL, tut_id INT NOT NULL, subject_id INT NOT NULL, batch_id INT NOT NULL, day TEXT NOT NULL, start_at TEXT NOT NULL, end_at TEXT NOT NULL, CONSTRAINT fk_availability_teachers FOREIGN KEY (tut_id) REFERENCES teachers(tut_id), CONSTRAINT fk_availability_subjects FOREIGN KEY (subject_id) REFERENCES subjects(subject_id), CONSTRAINT fk_availability_batches FOREIGN KEY (batch_id) REFERENCES batches(batch_id))