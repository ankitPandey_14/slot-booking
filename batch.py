import csv
import psycopg2

# connect to the database
con = psycopg2.connect(host='localhost',
                       dbname='details',
                       user='postgres',
                       password='@NK|T14P@sTgre',
                       port=5432,
                       )


# cursor
cur = con.cursor()

def insert_data(tbl, fl):
    cur.execute(f"SELECT * FROM {tbl}")
    rows = cur.fetchall()

    with open(fl, 'r') as cf:
        reader = csv.reader(cf)
        next(cf)
        l=list(reader)
        row=-1
        for row in range(len(rows)):
            if rows[row][0] == int(l[row][0]):
                pass
        else:
            for i in range(len(l)-len(rows)):
                cur.execute(f"INSERT INTO {tbl} VALUES (%s, %s)", l[row+1+i])
        

tbl = 'batches'
fl = 'batches_ins.csv'
insert_data(tbl, fl)

cur.execute(f'SELECT * FROM {tbl}')

rows = cur.fetchall()
for row in rows:
    print(row)


# commit the transaction
con.commit()

# close the cursor
cur.close()

# close the connection
con.close()
