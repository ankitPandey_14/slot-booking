import csv
import psycopg2

#connecting to the database
con = psycopg2.connect(host='localhost',
                       dbname='details',
                       user='postgres',
                       password='@NK|T14P@sTgre',
                       port=5432,
                       )  


#cursor
cur = con.cursor()

sub_nm = input("Enter your preferred subject name : ")
day = input("Enter your preferred week day name : ")

print(sub_nm)
print(day)

cur.execute("SELECT availability.start_at, tut_id FROM availability inner join subjects on availability.subject_id = subjects.subject_id where subjects.subject_name=(%s) and availability.day=(%s)", [sub_nm, day])


# cur.execute("SELECT availability.day FROM availability inner join subjects on availability.subject_id = subjects.subject_id where subjects.subject_name='Chemistry' and availability.start_at='9:00:00' and (availability.day='Monday' or availability.day='Tuesday' or availability.day='Thursday');")


row = cur.fetchall()
print("Your preferred time slots are vailable at : ")
for r in row:
    print( r[0])
    

book_tms = int(input("Enter the timing you want to book for (24 hour clock eg. 9 for 9AM, 10 - 10AM ... ) : "))

cur.execute("DELETE FROM availability where tut_id=(%s) and day=(%s) and start_at=(%s) and end_at=(%s)", [row[0][1], day, f"{book_tms}:00:00", f"{book_tms+1}:00:00"])

cur.execute("INSERT INTO timetable VALUES (%s, %s, %s, %s, %s)", [row[0][1], sub_nm, day, f"{book_tms}:00:00", f"{book_tms+1}:00:00"])


# commit the connection
con.commit()

# close the cursor
cur.close()

# close the connection
con.close()





# CREATE TABLE timeTable(tut_id INT NOT NULL, subject_name TEXT NOT NULL, day TEXT NOT NULL, start_at TEXT NOT NULL, end_at TEXT NOT NULL, CONSTRAINT fk_timeTable_teachers FOREIGN KEY (tut_id) REFERENCES teachers(tut_id));